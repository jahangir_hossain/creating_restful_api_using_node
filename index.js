const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const routers =  require('./routes/api');

//set up express app
const app = express();

//connect to mongodb
mongoose.connect('mongodb://localhost/studentList', { useNewUrlParser: true });
mongoose.Promise = global.Promise;


//use body-parser middleware
app.use(bodyParser.json());

//initialize routes
app.use('/api', routers); 

// adding error handler middleware
app.use(function(err, req, res, next){
    console.log(err);
    res.status(422).send({error: err.message});
});


//listen for a request
app.listen(process.env.port || 4000, function(){
    console.log('now listening for requests at port: 4000');
});
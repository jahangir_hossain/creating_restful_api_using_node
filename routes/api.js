const express = require('express');
const router = express.Router();
const Student = require('../models/student');

//get student list from database
router.get('/getStudentList', function(req, res, next){
    Student.find({}).then(function(students){
        res.send(students);
    }).catch(next);
});

// add a new student to the database
router.post('/addNewStudent', function(req, res, next){
    console.log(req.body);
    Student.create(req.body).then(function(student){
        res.send(student);
    }).catch(next);

});


// update a student in the database
router.put('/updateStudent/:id', function(req, res, next){
    Student.findByIdAndUpdate({_id: req.params.id}, req.body).then(function(){
        Student.findOne({_id: req.params.id}).then(function(student){
            res.send(student);
        });

    }).catch(next);
    
});

//delete a student

router.delete('/deleteStudent/:id', function(req, res, next){
    Student.findByIdAndRemove({_id: req.params.id}).then(function(student){
        res.send(student);
    }).catch(next);
    
});

module.exports = router;
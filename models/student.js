const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// creeate Student Schema and model

const StudentSchema = new Schema({
    name: {
        type: String,
        required: [true, 'Name fields is required']
    },
    department: {
        type: String
    },
    session:{
        type: Number
    },
    isActive:{
        type: Boolean
    }
});

const Student = mongoose.model('student', StudentSchema);

module.exports = Student;